minim :: [Int] -> Int
minim [x] = x
minim (x : xs) = if x < m then x else m
    where m = minim xs


levenshtein :: String -> String -> Int
levenshtein [] ys = length ys
levenshtein xs [] = length xs
levenshtein (x:xs) (y:ys)
    | x == y = levenshtein xs ys
    | otherwise = 1 + minim
        [
            levenshtein (x:xs) ys
            , levenshtein xs (y:ys)
            , levenshtein xs ys
        ]