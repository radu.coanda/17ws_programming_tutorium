import java.util.List;
import java.util.LinkedList;

public class Baum {
    private int wert;
    private Baum links, rechts;

    public Baum(Baum l, int wert, Baum r) {
        this.links = l;
        this.wert = wert;
        this.rechts = r;
    }

    public int hoehe() {
        int l = 0;
        if (this.links != null)
            l = this.links.hoehe();

        int r = 0;
        if (this.rechts != null)
            r = this.rechts.hoehe();
        if (l > r)
            return l + 1;
        else
            return r + 1;
    }

    public void einfuegen(int x) {
        Baum prev = null;
        Baum cur = this;
        while (cur != null) {
            prev = cur;
            if (x <= cur.wert)
                cur = cur.links;
            else
                cur = cur.rechts;
        }

        Baum neu = new Baum(null, x, null);
        if (x <= prev.wert)
            prev.links = neu;
        else
            prev.rechts = neu;
    }

    public List<Integer> toList() {
        List<Integer> res = new LinkedList<>();
        this.toList(res);
        return res;
    }

    private void toList(List<Integer> res) {
        if (this.links != null)
            this.links.toList(res);
        res.add(this.wert);
        if (this.rechts != null)
            this.rechts.toList(res);
    }

    public <T> T apply(BaumFolder<T> f) {
        T l;
        if (this.links != null) {
            l = this.links.apply(f);
        } else {
            l = f.handleNull();
        }
        T r;
        if (this.rechts != null) {
            r = this.rechts.apply(f);
        } else {

            r = f.handleNull();
        }
        return f.handleBaum(l, this.wert, r);
    }

}