public interface BaumFolder<T> {
    T handleNull();

    T handleBaum(T l, int w, T r);
}
