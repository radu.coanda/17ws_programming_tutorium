class BaumInc implements BaumFolder<Baum> {
    public Baum handleNull() {
        return null;
    }

    public Baum handleBaum(Baum links, int wert, Baum rechts) {
        return new Baum(links, wert + 1, rechts);
    }
}
