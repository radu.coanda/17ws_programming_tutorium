class BaumSum implements BaumFolder<Integer> {
    public Integer handleNull() {
        return 0;
    }

    public Integer handleBaum(Integer l, int w, Integer r) {
        return l + w + r;
    }
}
