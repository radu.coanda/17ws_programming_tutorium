public class BaumTest {

    public static void main(String[] args) {
        Baum l = new Baum(null, 24, null);
        Baum r = new Baum(null, 66, null);
        Baum testTree = new Baum(l, 43, r);

        // Height
        System.out.println(testTree.hoehe());

        // Insert
        testTree.einfuegen(34);
        testTree.einfuegen(100);
        testTree.einfuegen(0);
        System.out.println(testTree.hoehe());

        // toList
        System.out.println(testTree.toList());
    }
}