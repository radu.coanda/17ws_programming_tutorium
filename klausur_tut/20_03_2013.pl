% Knowledge Base
busFahrt(linie4, 1420, drieschergaesschen, 1424, karlsgraben).
busFahrt(linie33, 1411, ponttor, 1413, drieschergaesschen).

% Befehle:
% busVerbindung(ponttor, karlsgraben, 1400, P).
% busVerbindung(ponttor, karlsgraben, 1415, P).


% Aufgabe
busVerbindung(O, O, _, []).
busVerbindung(V, B, S, [mit(L, VZ, BO)|R]) :- busFahrt(L, VZ, V, BZ, BO),
                            VZ >= B,
                            busVerbindung(BO, B, BZ, R).