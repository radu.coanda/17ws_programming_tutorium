% Aufruf:
% del(2,[3,2,1,2,3],ZS).
% delN(s(s(0)),[1,2,3],ZS).
% min([3,2,1,2,3],Y).

% a
del(_, [], []).
del(X, [X|XS], XS).
del(X, [Y|XS], [Y|R]) :- del(X, XS, R).

% b
delN(0, XS, XS).
delN(s(X), XS, R) :- del(_, XS, YS), delN(X, YS, R).

% c
min([X], X).
min([X|XS], X) :- min(XS, Y), Y >= X.
min([X|XS], Y) :- min(XS, Y), Y < X.