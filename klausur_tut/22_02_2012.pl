% Aufruf
% kennzeichen([auto(eu-ts884,69),auto(dn-gh184,64),auto(ac-lj123,72)],X).
% einordnen([auto(ac-mb815,66),auto(bm-co100,74)],[auto(eu-ts884,69),auto(dn-gh184,64),auto(ac-lj123,72)],X).
%

% a
kennzeichen([], []).
kennzeichen([auto(X, _) | XS], [X| R]) :- kennzeichen(XS, R).

% b
einordnen(XS, [], XS).
einordnen([], YS, YS).
einordnen([X|XS], [Y|YS], [X, Y|R]) :- einordnen(XS, YS, R).

% c
durchschnitt([auto(_, G)], 1, G).
durchschnitt([auto(_, G) | XS], N, D) :- durchschnitt(XS, N1, D1), N is N1 + 1, D is (D1 * N1 + G) / N.