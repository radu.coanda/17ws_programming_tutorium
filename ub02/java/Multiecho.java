/**
 * Programm zum Einlesen einer positiven Zahl und eines Worts, welches das Wort
 * anschliessend so oft ausgibt, wie durch die Zahl festgelegt wurde.
 */
public class Multiecho {

    public static void main(String[] args) {
        // Einlesen der Zahl mit Ueberpruefung, dass die Zahl positiv ist:
        int zahl = 0;
        while (zahl < 1) {
          zahl = SimpleIO.getInt("Bitte geben Sie eine positive Zahl ein");
        }
        // Einlesen des Wortes:
        String wort = SimpleIO.getString("Bitte geben Sie ein Wort ein");
        // Ausgabe des Wortes so oft wie durch die Zahl festgelegt wurde:
        int i = 0;
        String multi = "";
        while (i < zahl) {
            multi += wort;
            i++;
        }
        SimpleIO.output(multi, "MultiEcho");
    }

}
