public class BubbleSort {

    public static int[] bubbleSort(int[] a) {
        int tmp = -1;
        for(int i = a.length - 1; i > 0; i--) {
            for(int j = 0; j < i; j++) {
                if(a[j] > a[j+1]) {
                    tmp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = tmp;
                }
            }
        }
        return a;
    }

    public static void main(String args[]) {
        int[] startArray = {23, 44, 2, 11, 20};
        
        // Print unsorted Array
        System.out.print("Unsortiertes Array: [");
        for(int i = 0; i < startArray.length - 1; i++) {
            System.out.print(startArray[i] + ", ");
        }
        System.out.print(startArray[startArray.length - 1] + "]\n");

        int[] sortedArray = bubbleSort(startArray);
        
        // Print sorted Array
        System.out.print("Sortiertes Array:   [");
        for(int i = 0; i < sortedArray.length - 1; i++) {
            System.out.print(sortedArray[i] + ", ");
        }
        System.out.print(sortedArray[sortedArray.length - 1] + "]\n");
    }
}