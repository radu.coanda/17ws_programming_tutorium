public class Main {

    public static void sammle ( Pilz [] wald , Mensch ... menschen ){
        int i = 0;
        for(Pilz pilz: wald) {
            while(i < menschen.length && !menschen[i].hatPlatz()) {
                i++;
            }

            if(i < menschen.length) {
                menschen[i].korb[menschen[i].anzahl] = pilz;
                menschen[i].anzahl++;
            }

        }

        for(Mensch mensch: menschen) {
            mensch.ausgabe();
        }
    }
 
    public static void main ( String [] args ) {
        Pilz steinpilz = new Pilz();
        steinpilz.name = "Steinpilz";
        Pilz champignon = new Pilz();
        champignon.name = "Champignon";
        Pilz pfifferling = new Pilz();
        pfifferling.name = "Pfifferling";
        Mensch bonnie = new Mensch();
        bonnie.name = "Bonnie";
        bonnie.korb = new Pilz[3];
        Mensch clyde = new Mensch();
        clyde.name = "Clyde";
        clyde.korb = new Pilz[4];
        
        Pilz [] wald = { steinpilz , champignon , champignon , pfifferling , steinpilz , pfifferling , champignon };
        sammle ( wald , bonnie , clyde );
     }
}
