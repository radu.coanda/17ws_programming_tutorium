public class Mensch {
    String name ;
    Pilz [] korb ;
    int anzahl = 0;

    public boolean hatPlatz() {
        boolean hatPlatz = false;
        if( anzahl < korb.length) {
            hatPlatz = true;
        }
        return hatPlatz;
    }

    public void ausgabe() {
        System.out.println(name + ":");
        for(Pilz pilz : korb) {
            if(pilz != null) {
                System.out.println(pilz.name);
            }
        }
    }
}
