public class Test {

    public static void main(String args[]) {
        Toolbox tools = new Toolbox("Toolbox 1", 10);
        System.out.println("-----------------------------------");
        System.out.println(tools.getName() + " enthaelt " + tools.getCapacity() + " freie Plaetze.");

        Tool t1 = Tool.Materials;
        tools.addTool(t1);
        Tool t2 = Tool.SimpleTool;
        tools.addTool(t2);
        Tool t3 = Tool.Materials;
        tools.addTool(t3);
        Tool t4 = Tool.PowerTool;
        tools.addTool(t4);
        Tool t5 = Tool.SimpleTool;
        tools.addTool(t5);
        Tool t6 = Tool.SimpleTool;
        tools.addTool(t6);
        Tool t7 = Tool.Materials;
        tools.addTool(t7);

        System.out.println("-----------------------------------");
        System.out.println(tools.getName() + " enthaelt " + tools.getCapacity() + " freien Platz.");
        tools.printAllTools();
        
        Toolbox tools2 = new Toolbox("Toolbox 2", t1, t2, t3, t4, t5, t6, t7);
        System.out.println("-----------------------------------");
        System.out.println(tools2.getName() + " enthaelt " + tools2.getCapacity() + " freie Plaetze.");
        tools2.printAllTools();
        
    }
}