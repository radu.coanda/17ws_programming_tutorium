disppublic class Toolbox {

    private String name;
    private int capacity;
    private static final int PowerToolSize = 3;
    private Tool[] tools;

    public Toolbox(String name, int capacity) {
        this.name = name;
        this.capacity = capacity;
        this.tools = new Tool[capacity];
    }

    public Toolbox(String name, Tool... tools) {
        this.name = name;
        this.capacity = 0;
        this.tools = tools;
        for(Tool tool: tools) {
            if(tool == null) {
                capacity += 1;
            }
        }
    }

    private boolean checkRoomForPowerTool(Wrapper w) {
        int counter = 0;

        if(this.tools == null || this.tools.length < Toolbox.PowerToolSize) {
            return false;
        }
        
        for(int i = 0; i < this.tools.length; i++) {
            if(this.tools[i] == null) {
                counter += 1;
            } else {
                counter = 0;
            }

            if(counter == Toolbox.PowerToolSize) {
                w.set(i - (Toolbox.PowerToolSize - 1));
                return true;
            }
        }

        return false;
    }

    public void addTool(Tool t) {
        switch(t) {
            case PowerTool:
                Wrapper w = new Wrapper(0);
                checkRoomForPowerTool(w);
                for(int i = 0; i < Toolbox.PowerToolSize; ++i) {
                    this.tools[w.get() + i] = t;
                }
                this.capacity -= Toolbox.PowerToolSize;
                break;
            case SimpleTool:
                for(int i = 0; i < this.tools.length; i++) {
                    if(this.tools[i] == null) {
                        this.tools[i] = t;
                        this.capacity -= 1;
                        break;
                    }
                }
                break;
            case Materials:
                for(int i = 0; i < this.tools.length; i++) {
                    if(this.tools[i] == null) {
                        this.tools[i] = t;
                        this.capacity -= 1;
                        break;
                    }
                    if(this.tools[i] == Tool.Materials) {
                        break;
                    }
                }
                break;
        }
    }

    public void printAllTools() {
        for(int i = 0; i < this.tools.length; i++) {
            System.out.println("[" + i + "]:  " + this.tools[i]);
        }
    }

    // GETTERS & SETTERS
    public int getCapacity() {
        return this.capacity;
    }

    public String getName() {
        return this.name;
    }

    public Tool getTool(int index) {
        if(this.tools.length > index && index >= 0 && this.tools[index] != null) {
            return this.tools[index];
        }
        return null;
    }

    public void setName(String name) {
        this.name = name;
    }
}