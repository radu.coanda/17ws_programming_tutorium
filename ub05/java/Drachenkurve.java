public class Drachenkurve {

        private static void kurveR(Canvas c, int order) {
		if(order <= 0) {
			c.drawForward(10);
		} else {
			kurveR(c, order-1);
			c.rotate(90);
			kurveL(c, order-1);
		}
	}	

	public static void kurveL(Canvas c, int order) {
		if(order <= 0) {
			c.drawForward(10);
		} else {
			kurveR(c, order-1);
			c.rotate(-90);
			kurveL(c, order-1);
		}
	}	
	
	public static void main(String[] args) {
		Canvas s = new Canvas();
		s.rotate(180); // Rotiert die aktuelle Ausrichtung nach oben
		kurveR(s, 3);
	}
}
