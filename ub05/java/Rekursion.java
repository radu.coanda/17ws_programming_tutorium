import java.util.Arrays;

public class Rekursion {

    public static int arraySum(int[] a) {
        int res = 0;
        for (int i = 0; i < a.length; i++) {
            res += a[i];
        }
        return res;
    }

    public static int recursiveArraySum(int[] a) {
        if(a == null || a.length < 0) {
            return 0;
        }

        if(a.length == 1) {
            return a[0];
        }

        return a[a.length - 1] + recursiveArraySum( /*cutArray(a, 0, a.length - 2)*/ Arrays.copyOfRange(a, 0, a.length - 1) );
    }

    private static int[] cutArray(int[] a, int i, int j) {
        // test
        if(i > j && a.length < j) {
            return null;
        }
        // new array
        int[] b = new int[j - i + 1];
        // fill
        for(int q = 0; q < b.length; q++) {
            b[q] = a[i + q];
        }
        
        return b;
    }

    public static void main(String[] args) {
        int[] myArray = {1,2,3,4,5,6};
        int arraySum = arraySum(myArray);
        int recursiveArraySum = recursiveArraySum(myArray);
        
        System.out.println(arraySum + " - " + recursiveArraySum);
    }
}