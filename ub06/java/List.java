public class List {
    public static final List EMPTY = new List(null, 0);
    private int value;
    private List next;

    public List(List n, int v) {
        this.next = n;
        this.value = v;
    }

    public List getNext() {
        return this.next;
    }

    public int getValue() {
        return this.value;
    }

    // Teil b
    public int length() {
        if(this.next == List.EMPTY) {
            return 1;
        }
        return 1 + this.next.length();
    }

    public String toString() {
        if(this.next == List.EMPTY) {
            return this.value + ";";
        }
        return this.value + ", " + this.next.toString();
    }

    // Teil c
    public List getSublist(int i) {
        if(i == 0) {
            return this.EMPTY;
        }

        return new List(this.next.getSublist(i-1), this.value);
    }
}