public class Main {

    public static void main(String[] args) {
        List myList = new List(List.EMPTY, 5);
        List myList2 = new List(myList, 4);
        List myList3 = new List(List.EMPTY, 1);
        List myList4 = new List(myList3, 0);

        System.out.println(myList2.length());
        System.out.println(myList2);
        System.out.println(myList2.getSublist(1).toString());

        List mergeList = ListExercise.merge(myList2, myList4);
        System.out.println(mergeList);
    }
}