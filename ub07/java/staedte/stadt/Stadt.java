// Package
package stadt;

// Import
import stadt.buerger.*;
import zufall.Zufall;

public class Stadt {
	private Buerger[] einwohner;
	
	public Stadt(int einwohnerZahl) {
		this.einwohner = new Buerger[einwohnerZahl];
		for (int i = 0; i < this.einwohner.length; ++i) {
			String name = Zufall.name();
			Buerger b;
			switch (Zufall.zahl(5)) {
			case 0:
				b = new Dieb(name);
				break;
			case 1:
				b = new ReicherBuerger(name, Zufall.zahl(1000));
				break;
			case 2:
				b = new Polizist(name);
				break;
			case 3:
				b = new Gefangener(name);
				break;
			case 4: default:
				b = new Buerger(name);
			}
			this.einwohner[i] = b;
		}
	}

	public static void main(String[] args) {
		Stadt stadt = new Stadt(10);
		for (int i = 0; i < 10; ++i) {
			Buerger b = stadt.einwohner[Zufall.zahl(stadt.einwohner.length)];
			b.aktion(stadt.einwohner);
		}
	}
}
