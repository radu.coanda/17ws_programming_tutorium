// Package
package stadt.buerger;

public class Buerger {
	private String name;

	public Buerger(String name) {
		this.name = name;
	}

	public boolean hatDiebesgut() {
		return false;
	}

	public String toString() {
		return this.name;
	}

	public void aktion(Buerger[] einwohner) {
		System.out.println("Buerger " + name + " geht spazieren.");
	}

	public String getName() {
		return this.name;
	}
}
