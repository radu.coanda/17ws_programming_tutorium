// Package
package stadt.buerger;

// Import
import zufall.Zufall;

public class Dieb extends Buerger {
	private int diebesgut;

	public Dieb(String name) {
		super(name);
		diebesgut = 0;
	}

	public boolean hatDiebesgut() {
		return this.diebesgut > 0;
	}

	public void aktion(Buerger[] einwohner) {
		System.out.println("Dieb " + this + " sucht nach Diebesgut.");
		for (int i = 0; i < 5; ++i) {
			Buerger b = einwohner[Zufall.zahl(einwohner.length)];
			if (b instanceof ReicherBuerger) {
				ReicherBuerger rb = (ReicherBuerger) b;
				if (rb.getReichtum() <= 1) {
					continue;
				}
				int geld = Zufall.zahl(rb.getReichtum());
				this.diebesgut += geld;
				rb.setReichtum(rb.getReichtum() - geld);
				System.out.println("Dieb " + this + " klaut " + rb + " " + geld
						+ " Euro.");
				return;
			} else if (b instanceof Polizist) {
				System.out.println("Dieb " + this + " bricht die Suche ab.");
				return;
			}
		}
	}
}
