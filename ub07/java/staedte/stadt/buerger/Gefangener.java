// Package
package stadt.buerger;

public class Gefangener extends Dieb {
	public Gefangener(String name) {
		super(name);
	}

	public void aktion(Buerger[] einwohner) {
		System.out.println("Gefangener " + this.getName()
				+ " aergert sich im Gefaengnis.");
	}
}
