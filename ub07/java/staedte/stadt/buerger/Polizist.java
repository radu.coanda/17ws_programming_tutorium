// Package
package stadt.buerger;

public class Polizist extends Buerger {

	public Polizist(String name) {
		super(name);
	}

	public void aktion(Buerger[] einwohner) {
		System.out.println("Polizist " + this + " geht auf Verbrecherjagd.");
		for (int i = 0; i < einwohner.length; ++i) {
			Buerger b = einwohner[i];
			if (b.hatDiebesgut()) {
				System.out.println("Polizist " + this + " entlarvt Dieb " + b
						+ ".");
				einwohner[i] = new Gefangener(b.getName());
				System.out.println("Dieb " + b + " wurde eingesperrt.");
			}
		}
	}
}
