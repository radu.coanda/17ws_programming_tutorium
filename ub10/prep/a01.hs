-- Constructor
data Mobile a = Stern | Seepferdchen | Elefant (Mobile a) 
                        | Kaenguru a (Mobile a) (Mobile a) deriving Show 

mobileLinks :: Mobile Int
mobileLinks =   Kaenguru 1
                        ( Elefant ( Kaenguru 2
                            Stern
                            ( Kaenguru 3
                                Seepferdchen
                                Stern
                            )
                        ))
                    Seepferdchen
                    
mobileRechts :: Mobile Int
mobileRechts = Elefant ( Kaenguru 1 ( Elefant Stern ) ( Elefant Seepferdchen ))

-- Part b:
count :: Mobile a -> Int
count Stern             = 1
count Seepferdchen      = 1
count (Elefant m)       = 1 + count m
count (Kaenguru _ m n)  = 1 + count m + count n

-- Part c:
liste :: Mobile a -> [a]
liste Stern             = []
liste Seepferdchen      = []
liste (Elefant m)       = liste m
liste (Kaenguru x m n)  = x : liste m ++ liste n

-- Part d:
greife :: Mobile a -> Int -> Mobile a
greife e 1                  = e
greife (Elefant m) x        = greife m (x-1)
greife (Kaenguru _ m n) x   | (x-1) <= count m  = greife m (x-1)
                            | otherwise         = greife n (x-1- (count m))