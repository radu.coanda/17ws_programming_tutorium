-- Part f
f [] x y            = y
f [ z : zs ] x y    = f [] ( z : x ) y

-- Part g
g x 1 = 1
g x y = (\ x -> ( g x 0)) y

-- Part h
h ( x : xs ) y z = if x then h xs x ( y : z ) else h xs y z

-- Part i
data T a b = C0 | C1 a | C2 b | C3 ( T a b ) ( T a b )

i ( C3 ( C1 x ) ( C2 y ))           = C2 0
i ( C3 ( C1 ( x : xs )) ( C2 y ))   = i ( C3 ( C1 [ y ]) ( C2 x ))
