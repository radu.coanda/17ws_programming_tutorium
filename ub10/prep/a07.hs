-- Part a:
odds :: [Int]
odds = 1 : map (+2) odds

-- Part b:
primeFactor :: Int -> [Int]
primeFactor = pHelper primes
    where
        pHelper (x:xs) y    | y == 1        = []
                            | mod y x == 0  = x : pHelper (x:xs) (div y x)
                            | otherwise     = pHelper xs y

-- Helper Functions:
from :: Int-> [Int]
from x = x : from (x+1)

drop_mult :: Int -> [Int] -> [Int]
drop_mult x xs = filter (\y -> mod y x /= 0) xs                            

dropall :: [Int] -> [Int]
dropall (x:xs) = x : dropall (drop_mult x xs)

primes :: [Int]
primes = dropall (from 2)