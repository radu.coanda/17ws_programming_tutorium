-- Constructor
data Mobile a =  deriving Show 

-- Examples
mobileLinks :: Mobile Int
mobileLinks =   Kaenguru 1
                        ( Elefant ( Kaenguru 2
                            Stern
                            ( Kaenguru 3
                                Seepferdchen
                                Stern
                            )
                        ))
                    Seepferdchen
                    
mobileRechts :: Mobile Int
mobileRechts = Elefant ( Kaenguru 1 ( Elefant Stern ) ( Elefant Seepferdchen ))

-- Part b:
count :: Mobile a -> Int


-- Part c:
liste :: Mobile a -> [a]


-- Part d:
greife :: Mobile a -> Int -> Mobile a
