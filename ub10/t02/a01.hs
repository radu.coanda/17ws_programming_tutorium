-- Constructor
data Mobile a = Stern | Seepferdchen | Elefant (Mobile a) | Kaenguru a (Mobile a) (Mobile a) deriving Show 

-- Examples
mobileLinks :: Mobile Int
mobileLinks =   Kaenguru 1
                        ( Elefant ( Kaenguru 2
                            Stern
                            ( Kaenguru 3
                                Seepferdchen
                                Stern
                            )
                        ))
                    Seepferdchen
                    
mobileRechts :: Mobile Int
mobileRechts = Elefant ( Kaenguru 1 ( Elefant Stern ) ( Elefant Seepferdchen ))

-- Part b:
count :: Mobile a -> Int
count Stern                 = 1
count Seepferdchen          = 1
count (Elefant m)           = 1 + count m
count (Kaenguru _ m1 m2)    = 1 + count m1 + count m2

-- Part c:
liste :: Mobile a -> [a]
liste Stern = []
liste Seepferdchen = []
liste (Elefant m) = liste m
liste (Kaenguru v m1 m2) = v : liste m1 ++ liste m2


-- Part d:
greife :: Mobile a -> Int -> Mobile a
greife e 1 = e
greife (Elefant m) n = greife m (n-1)
greife (Kaenguru _ m1 m2) n | (n-1) <= count m1 = greife m1 (n-1)
                            | otherwise         = greife m2 (n - 1 - count m1)
