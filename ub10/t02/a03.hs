-- Part f
f :: a -> b -> c -> d
f :: [a'] -> b -> c -> d
f :: [a'] -> b -> d -> d
f :: [a'] -> [a'] -> d -> d
f [] x y            = y
f [ z : zs ] x y    = f [] ( z : x ) y

-- Part g
g :: a -> b -> c
g :: a -> Int -> Int
g :: Int -> Int -> Int
g x 1 = 1
g x y = (\ x -> ( g x 0)) y

-- Part h
h :: a -> b -> c -> d
h :: [Bool] -> Bool -> [Bool] -> d
h ( x : xs ) y z = if x then h xs x ( y : z ) else h xs y z

-- Part i
data T a b = C0 | C1 a | C2 b | C3 ( T a b ) ( T a b )

i :: m -> n
i :: T m1 m2 -> T n1 n2
i :: T m1 m2 -> T n1 Int
i :: T [m1'] m2 -> T n1 Int
i :: T [m1'] m1' -> T n1 Int

i ( C3 ( C1 x ) ( C2 y ))           = C2 0
i ( C3 ( C1 ( x : xs )) ( C2 y ))   = i ( C3 ( C1 [ y ]) ( C2 x ))
