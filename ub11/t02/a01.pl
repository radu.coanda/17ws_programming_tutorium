% Part (a)
% Test Aufruf: increment(node(leaf(s(0)),s(s(0)),leaf(0)),Res).
increment(leaf(X), leaf(s(X))).
increment(node(L, V, R), node( ResL, s(V), ResR )) :- increment(L, ResL), increment(R, ResR). 

% Part (b)
% Test Aufruf: append([a,b,c],[d,e],Res).
append([], YS, YS).
append([X|XS], YS, [X|Res]) :- append(XS, YS, Res).

% Part (c)
% Test Aufruf: inorder(node(leaf(s(0)),s(s(0)),node(leaf(s(0)),0,leaf(s(s(s(0)))))), Res).
inorder(leaf(X), [X]).
inorder(node(L, V, R), Res) :- inorder(L, ResL), inorder(R, ResR), append(ResL, [V|ResR], Res).