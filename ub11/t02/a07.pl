% prime(N) ist genau dann wahr, wenn N > 1 und N eine Primzahl ist.
prime(N) :- N > 1, X is N div 2, nodivisors(N, X).

% nodivisors(N, X) ist genau dann wahr, wenn  N keine Teiler zwischen X und 2 hat.
nodivisors(_, 1).
nodivisors(N, X) :- notdivisor(N, X), Y is X - 1, nodivisors(N, Y).

% notdivisor (N , X ) ist wahr gdw . N nicht durch X teilbar ist .
notdivisor(N, X) :- Y is N mod X, Y > 0.